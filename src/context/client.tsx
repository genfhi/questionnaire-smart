import createHTTPClient from "@iguhealth/client/lib/http";
import { createContext } from "react";

export default createContext<ReturnType<typeof createHTTPClient> | undefined>(
  undefined
);
